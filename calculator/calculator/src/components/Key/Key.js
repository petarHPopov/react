import React from 'react';
import PropTypes from 'prop-types';
import './Key.css';

const Key = ({ handleKeyPress, keyAction, keyType, keyValue }) => {
    const keyClass = `key-container ${keyType}`;

    const act = () => {
        return keyAction(keyValue);
    }

    const pres = (evt) => {
        return handleKeyPress(evt)
    }

    return (
        <div className={keyClass} onClick={act} onKeyPress={pres}>
            <p className="key-value">{keyValue}</p>
        </div>
    );
};

Key.propTypes = {
    handleKeyPress: PropTypes.func.isRequired,
    keyAction: PropTypes.func.isRequired,
    keyType: PropTypes.string.isRequired,
    keyValue: PropTypes.string.isRequired,
};

Key.defaultProps = {
    keyType: 'default',
    keyAction: 'default',
};

export default Key;